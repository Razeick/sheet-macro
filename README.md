## Troubleshooting
If your players cannot drag their actors at all, they lack the 'Create New Tokens' permission for actors that they own. This must be enabled for the module to function correctly.
