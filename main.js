Hooks.on('hotbarDrop', async (bar, data, slot) => {
	if (data.type !== 'Actor') {
		return;
	}

	const actor = game.actors.get(data.id);
	if (!actor) {
		return;
	}

	const command = `game.actors.get('${data.id}')?.sheet.render(true);`;
	let macro =
		game.macros.entities.find(macro => macro.name === actor.name && macro.command === command);

	if (!macro) {
		macro = await Macro.create({
			name: actor.name,
			type: 'script',
			img: actor.data.img,
			command: command
		}, {renderSheet: false});
	}

	game.user.assignHotbarMacro(macro, slot);
	return false;
});
